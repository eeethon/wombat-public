# aio version 10

###############################################################################
# Build AIO library (libaio.a)

# On Itasca
module load intel impi/intel

cd src
make clean
make


###############################################################################
# Build and Run Tests
#
cd test
make clean
make

./run_test_aio
./file_ext
./run_amr_output

# Compare STDOUT from these three commands with correct output in he files
# run_test_aio.out
# file_ext.out
# run_amr_output.out


###############################################################################
# Files in this distribution:

# In ./src
Makefile              Makefile to build aio library (libaio.a)
aio.f                 AIO implementation (includes IO timers)
aio_client_data.h     AIO internal client data
switched_io.c         Switched_io routines (called by AIO)
inmem_tree.c          In-memory block tree data reformatting routines (called by AIO)
avminmax.f            Optimized blending routines (called by inmem-tree)
mnt.c                 TCP/IP socket code (called by switched_io)
mnt.h                 Socket code header file
util_ze.c             FORTRAN wrapper functions for Z-lib routines

# In ./lib
libz.a                Z-lib memory-to-memory routines (called by AIO)
libaio.a              AIO statically linked library (build by running make in src directory)

# In ./test
Makefile              Makefile to build all three test applications
test_aio.f            Test & examples of how to use a mix of AIO routines (old and new)
file_ext.f            Test & examples of how to set name extentions for real data and meta-data files
amr_output.f          Test & example of how to use AIO for AMR output
amr_output.h          Header file for amr_output code

run_test_aio          Run script for test_aio
run_amr_output        Run script for amr_output
input_just_workers    Input file for test_aio
input_file_all_ranks  Input file for test_aio
amr_output.in         Input file for amr_output
run_test_aio.out      Correct output from run_test_aio script
file_ext.out          Correct output from file_ext application
run_amr_output.out    Correct output from run_amr_output script

###############################################################################
# New AIO user routines

      subroutine aio_set_data_extention_range(cExtLow, cExtHigh) 
      subroutine aio_set_meta_extention(cExtMeta)  
      subroutine aio_set_verbosity(verbosity)  
      subroutine aio_set_max_data_size(iMaxBuf)  
      subroutine aio_set_data_extention(iServerNumber) 
      subroutine aio_gen_data_file_name(cRoot, cDataFile)  
      subroutine aio_set_clients_to_low_ranks(nworkers, comm)  
      subroutine aio_write_buffer(cFullPath, nbytes, buffer, handle) 
      subroutine aio_write_buf_meta(cFullPath,nbuf,nmeta,buf,ibuf,ic) 


# Synopses:
      
#-------------------------------------------------------------------
      subroutine aio_set_data_extention_range(cExtLow, cExtHigh) 
      character*(*) cExtLow   ! Lowest (1st) aio data file extention
      character*(*) cExtHigh  ! Max possible aio data file extention
        Default: cExtLow="-000" ;   cExtHigh="-999"
        Available range: 0 to Z = {0,...,9, a,...,z, A,...,Z}
        Examples (n = number of fields used):
          Decimal: "-000"    "-999"     up to 10^n servers
          Alpha:   "A"       "Z"        up to 26^n servers 
          Hex:     "-00.dat" "-ff.dat"  up to 16^n servers
          Binary:  "--0000"  "--1111"   up to  2^n servers
          Compact: "_00"     "_ZZ"      up to 62^n servers
      
#-------------------------------------------------------------------
      subroutine aio_set_meta_extention(cExtMeta)  
      character*(*) cExtMeta  ! aio meta-data file extention
       Default: cExtMeta=".meta"
      
#-------------------------------------------------------------------
      subroutine aio_set_verbosity(verbosity)  
      integer verbosity       ! aio verosity level
       Controls diagnostic output written by AIO servers to STDOUT
         verbosity  = 0   No diagnostic output (default)
         verbosity >= 1   AIO servers report at start-up only
         verbosity >= 2   AIO servers report IO timing info
         verbosity >= 10  Detailed AIO debugging information
     
#-------------------------------------------------------------------
      subroutine aio_set_max_data_size(iMaxBuf)  
      integer iMaxBuf        ! Maximum aio data buffer size [bytes]
       Default:  iMaxBuf = 10000000   (10 MB)
      
#-------------------------------------------------------------------
      subroutine aio_set_data_extention(iServerNumber) 
      integer iServerNumber  ! Number of aio server (0,...,nservers-1)
        Generates aio extention for files written by this aio server
      
#-------------------------------------------------------------------
      subroutine aio_gen_data_file_name(cRoot, cDataFile)  
      character*(*) cRoot     ! Root name
      character*(*) cDataFile ! Full name for this server or client
        Generage full (root+extention) file name written by aio server
      
#-------------------------------------------------------------------
      subroutine aio_set_clients_to_low_ranks(nworkers, comm)  
      integer nworkers     ! Number of worker ranks
      integer comm         ! MPI communicator
      !  Ranks 0,nworkers-1     will be aio clients
      !  Ranks nworkers,nranks  will be aio wervers
      
#-------------------------------------------------------------------
      subroutine aio_write_buffer(cFullPath, nbytes, buffer, handle) 
      character*(*) cFullPath  ! file name, or path to file
      integer nbytes           ! NUmber of bytes in buffer
      byte buffer(*)           ! Pointer to data buffer
      integer handle           ! AIO handle for write request, range: [1, 100]
       Initiates an asyncronous write.
       Data in buffer will be written into file cFullPath with data from other AIO clients.
       use aio_wait(handle) to wait till "buffer" is free.
      
#-------------------------------------------------------------------
      subroutine aio_write_buf_meta(cFullPath,nbuf,nmeta,buf,ibuf,ic) 
      character*(*) cFullPath  ! file name, or path to file
      integer nbuf             ! NUmber of bytes of real data in buf
      integer nmeta            ! NUmber of bytes of meta-data in buf
      byte buf(*)              ! Pointer to buffer with real and meta-data
      integer ibuf             ! AIO handle for write request, range: [1, 100]
      integer ic               ! Meta-data output control
       Initiates an asyncronous write of real data with meta-data.
       Meta-data must all be at the end of the buf
       use aio_wait(handle) to wait till real data is written and "buf" is free.
       ic = 0: keep collecting meta-data
       ic = 1: collect meta-data in this call and write to file with detaul meta-data file extension
       ic = 2: write meta-data to file name provided in this cal

