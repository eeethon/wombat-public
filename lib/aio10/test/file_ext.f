
      character*20 cRootName, cFullName
      cRootName = "ab010-0123          "

      write (6,*) "Use Defualt aio adta file extetions: -000 to -999"
      do iServer = 0, 220
        call aio_set_data_extention(iServer)
        call aio_gen_data_file_name(cRootName, cFullName)
        write (6,999) iServer, trim(cFullName)
      enddo
999   format("AIO server #:",i3, "    Data file name: ", a)
        
      write (6,*) " "
      write (6,*) "Use file extetions in range: aa to zz"
      call aio_set_data_extention_range("aa", "zz")
      do iServer = 0, 30
        call aio_set_data_extention(iServer)
        call aio_gen_data_file_name(cRootName, cFullName)
        write (6,999) iServer, trim(cFullName)
      enddo

      write (6,*) " "
      write (6,*) "Use file extetions in range: -00 to ZZ"
      call aio_set_data_extention_range("-00", "-ZZ")
      do iServer = 0, 80
        call aio_set_data_extention(iServer)
        call aio_gen_data_file_name(cRootName, cFullName)
        write (6,999) iServer, trim(cFullName)
      enddo

      stop
      end
