# README #
 
Wombat is a magnetohydrodynamic code for simulating astrophysical fluids.  It should be fast and accurate.  Let us know if that is not true.
Wombat can be used for production science (we do), but given the experimental design it is not as user friendly as we'd like yet.  This is
being addressed so please monitor this repository for updates.  There is an issue tracker that users can post problems to as well.  There
are also references in a few places to features that will be available in upcoming releases but are not yet in place.  These include a
gravity solver, dark matter solver, and cosmic ray feedback.  For now both MHD and passive variables are fully implemented.  Also note that
at the moment restart files are not working.  These are coming very soon.

### How do I build Wombat? ###

Wombat is constantly being tested for the GNU, Intel and Cray compilers.  Best performance is obtained with the Cray compiler
followed by Intel and GNU.

To compile Wombat you should first select a problem source file from the "src/problems" directory.  Then edit "Config" in this
directory setting "PROBLEM" equal to the basename of your selected problem source file.  For example:

    PROBLEM = Ryu_Jones_95.f90

Next look in the "arch" directory for a *.csh or *.sh file that matches the architecture you are on.  If you find a match source that
file as it will setup your enviroment properly for both building and execution.  Once you have edited "Config" as needed simply execute:

    make ARCH=[architecture]

Where architecture is one of:

* CCE.cray
* INTEL.cray
* GNU.cray
* INTEL.linux
* GNU.linux
* INTEL.msi

Additional architectures can be added to the "arch/" directory.  An executable name "wombat" will be placed into this directory.

To build the e3d analysis toolset that is required for looking at the data coming out of Wombat:

    make e3d ARCH=[architecture]

where architecture is one of the options listed above.

If you need a debug build of Wombat to track down issues:

    make debug ARCH=[architecture]

This will enable run-time checks, and Wombat will run quite slowly as optimizations are also disbaled.

### How do I run Wombat? ###

#### Namelists ####

Wombat requires a namelist file to drive how it runs.  The "namelists" directory contains a script "gen-namelists" that
can be used to generate a starting point namelist.  For example, to create a namelist for running the Ryu & Jones (1995)
problem 2a shock tube in 3d:

    cd namelists
    ./gen-namelists Ryu_Jones_2a_0_3d

This produces a namelist called "Ryu_Jones_2a_0_3d" that can be passed as the only argument to "wombat."  To see a list
of available problem namelists:

    cd namelists
    ./gen-namelists avail

#### Simulation Setup Details ###

Most predefined problem setups, such as Ryu_Jones_2a, can be run in 1, 2 or 3d with any decomposition.  Users can modify
the generated namelist as they wish for any core count and domain size.  The namelist entries each has a brief description
next to it, and we will highlight the parameters users typically will modify.

* ndims

	An integer value of 1, 2 or 3 for the number of dimensions

* nranks_x, nranks_y, nrank_z

	An integer value for the number of MPI ranks along the X, Y or Z dimension

* naio

	The number of additional MPI ranks dedicated to writing output files.  Valid values are >= 0 < total number of ranks

* nthreads

	The number of OpenMP threads to use per MPI rank.  Users are unlikely to find good performance on clusters using threads due
	to poor performance of MPI_THREAD_MULTIPLE in most MPI libraries.  They are still encouraged to try as libraries other than
	Cray MPICH are making progress.

* patch_mailboxes_pslot

	The number of "mailboxes" for communication to any neighbor.  Each mailbox is large enough to communicate all 
	boundaries for a Patch.  Increasing this value consumes more memory but also will yield better performance.  For
	simulations not limited by memory this should be set to MAX(n_xpatches * n_ypatches, n_xpatches * n_zpatches, n_ypatches * nzxpatches)

* max_inflightcomms

	The number of large message size MPI_GET operations in flight at any moment for an MPI rank.  Some MPI libraries do not
	perform well with lots of messages in flight.  In general users should start with this set to 0 (unlimited) and then experiment 
	with values between 16 and 256 by powers of 2.

* nspins

	The number of polling spins to perform before moving to other work when waiting for ready signals from neighbors.  For systems
	with large network latencies or for large runs this can be used for tuning.  Users should start with this set to 1 and try values
	up to 100 by some increment to see what performs best.

* n_xpatches, n_ypatches, n_zpatches

	The number of Patches in X, Y or Z that an MPI rank's domain is decomposed into.  When using threads the product of these 3 vslues
	should be at least as large as the number of threads.  If that product cannot be evenly divided by the number of threads users are
	encouraged to try to keep MOD(n_xpatches * n_ypatches * n_zpatches, nthreads) > nthreads/2 for best performance.

* patch_nx, patch_ny, patch_nz

	The number of zones in X, Y or Z in a Patch.  Best performance is typically found with these set to 32 except on KNL, where a value
	of 48 performs best.  To determine the full domain size of a simulation along X (same can be repeated for other dimensions) do
	N_x = nranks_x * n_xpatches * patch*nx

* patch_nb

	The number of boundary zones in a Patch.  The directionally unsplit MHDTVD solver currebtly implemented requires this be set to 5
	or larger (only 5 will be used).  For MPI developers looking to tune message sizes in performance studies this parameter can be varied
	to increase messages sizes without affecting the simulation.

To execute Wombat first source the appropriate environment file sourced when building above.  Then depending on the systen you are one users will 
typically do one of the following:

* On Crays

        aprun -n [# ranks] -d [# threads] ./wombat [namelist file]

* On clusters

        mpirun -n [# ranks] ./wombat [namelist file]

Do note that when using threads users also need to take care of setting proper affinity so that threads/ranks are all laid out sequentially.
Each MPI stack and compiler may have different menthods for this.  We will add documentation for these in later updates.  On Cray systems the
instructions above will automatically handle thread affinity.

### Contribution guidelines ###

All developers are welcome to create new problem setups and add them to the repository as they see fit.  Code contributions
are by invitation only.

### Who do I talk to? ###

Peter Mendygral is the primary developer and architect.  Contact him at pjm@cray.com.

### How do I cite Wombat? ###

Papers using Wombat can be found in the code Wiki at https://bitbucket.org/pmendygral/wombat-public/wiki/Publications