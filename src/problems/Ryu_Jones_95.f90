#include "../config.h"

MODULE Mod_Problem

!######################################################################
!#
!# FILENAME: mod_RJ95_tests.f90
!#
!# DESCRIPTION: This module provides a class for initializing a domain
!#   and setting boundary values for running MHD shock tube tests from
!#   Ryu & Jones 95 in 1,2 or 3-dimensions.
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    4/10/2015  - Brian O'Neill
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Patch
USE Mod_ProblemBase

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: Problem

!### define a data type for this module ###
TYPE, EXTENDS(ProblemBase) :: Problem

    !### default everything as private ###
    PRIVATE

    CHARACTER*30 :: init_routine, bounds_routine
    REAL(WRD), PUBLIC :: gamma

    !### user-added problem data structres ###
    !### left/right state variables: 1-density ; 2,3,4-velocity ; 5,6,7-Bfield ; 8-pressure
    REAL(WRD), DIMENSION(:,:), ALLOCATABLE :: left, right
    REAL(WRD) :: phi, theta, psi, A(2,2), Ainv(2,2), AA(3,3), AAinv(3,3)
    INTEGER(WIS) :: nstate = 8

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE, PUBLIC :: destroy
    PROCEDURE, PUBLIC :: patch_init
    PROCEDURE, PUBLIC :: patch_bounds
    PROCEDURE :: initproblem
    PROCEDURE :: test_RJ95_1a
    PROCEDURE :: test_RJ95_1b
    PROCEDURE :: test_RJ95_2a
    PROCEDURE :: test_RJ95_2b
    PROCEDURE :: test_RJ95_3a
    PROCEDURE :: test_RJ95_3b
    PROCEDURE :: test_RJ95_4a
    PROCEDURE :: test_RJ95_4b
    PROCEDURE :: test_RJ95_4c
    PROCEDURE :: test_RJ95_4d
    PROCEDURE :: test_RJ95_5a
    PROCEDURE :: test_RJ95_5b
    PROCEDURE :: init_tube_states

END TYPE Problem

!### create an interface to the constructor ###
INTERFACE Problem

    MODULE PROCEDURE constructor

END INTERFACE Problem

!### object methods ###
CONTAINS

!### constructor for the class ###
!###   arguments are: the namelist file contents for grabbing parameters out of ###
FUNCTION constructor(namelist, nthreads, rankloc, simunits)

    TYPE(Problem) :: constructor
    CHARACTER*(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits

    CALL constructor%initproblem(namelist, nthreads, rankloc, simunits)
    RETURN

END FUNCTION constructor

!### this function will init the object and configure init and bound routine parameters ###
SUBROUTINE initproblem(self, namelist, nthreads, rankloc, simunits)

    CLASS(Problem) :: self
    CHARACTER*(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    CHARACTER*30 :: init_routine, bounds_routine
    REAL(WRD) :: gamma, phi, theta, psi

    NAMELIST /ProblemSetup/ init_routine, bounds_routine, gamma, phi, theta, psi

    !### set the routines we'll call ###
    READ(namelist, NML=ProblemSetup)
    self%init_routine   = init_routine
    self%bounds_routine = bounds_routine

    !### we handle setting the adiabatic index ###
    self%gamma = gamma

    ALLOCATE(self%left(self%nstate, nthreads), self%right(self%nstate, nthreads))

    !### user-added values ###
    self%phi   = phi   * simunits%pi / 180.0_WRD
    self%theta = theta * simunits%pi / 180.0_WRD
    self%psi   = psi   * simunits%pi / 180.0_WRD

    self%A(1,1) = COS(self%phi)
    self%A(1,2) = SIN(self%phi)
    self%A(2,1) = -SIN(self%phi)
    self%A(2,2) = COS(self%phi)

    self%Ainv(1,1) = COS(self%phi)
    self%Ainv(1,2) = -SIN(self%phi)
    self%Ainv(2,1) = SIN(self%phi)
    self%Ainv(2,2) = COS(self%phi)

    !### 3d rotation: Euler angle rotation matrix (e.g. Goldstein p.153)

    self%AA(1,1) = DCOS(self%psi)*DCOS(self%phi) - DCOS(self%theta)*DSIN(self%phi)*DSIN(self%psi)
    self%AA(1,2) = DCOS(self%psi)*DSIN(self%phi) + DCOS(self%theta)*DCOS(self%phi)*DSIN(self%psi)
    self%AA(1,3) = DSIN(self%psi)*DSIN(self%theta)
    self%AA(2,1) =-DSIN(self%psi)*DCOS(self%phi) - DCOS(self%theta)*DSIN(self%phi)*DCOS(self%psi)
    self%AA(2,2) =-DSIN(self%psi)*DSIN(self%phi) + DCOS(self%theta)*DCOS(self%phi)*DCOS(self%psi)
    self%AA(2,3) = DCOS(self%psi)*DSIN(self%theta)
    self%AA(3,1) = DSIN(self%theta)*DSIN(self%phi)
    self%AA(3,2) =-DSIN(self%theta)*DCOS(self%phi)
    self%AA(3,3) = DCOS(self%theta)

    !### inverse 3d rotation: transpose of Euler angle rotation matrix (e.g. Goldstein p.153)

    self%AAinv(1,1) = DCOS(self%psi)*DCOS(self%phi) - DCOS(self%theta)*DSIN(self%phi)*DSIN(self%psi)
    self%AAinv(1,2) =-DSIN(self%psi)*DCOS(self%phi) - DCOS(self%theta)*DSIN(self%phi)*DCOS(self%psi)
    self%AAinv(1,3) = DSIN(self%theta)*DSIN(self%phi)
    self%AAinv(2,1) = DCOS(self%psi)*DSIN(self%phi) + DCOS(self%theta)*DCOS(self%phi)*DSIN(self%psi)
    self%AAinv(2,2) =-DSIN(self%psi)*DSIN(self%phi) + DCOS(self%theta)*DCOS(self%phi)*DCOS(self%psi)
    self%AAinv(2,3) =-DSIN(self%theta)*DCOS(self%phi)
    self%AAinv(3,1) = DSIN(self%theta)*DSIN(self%psi)
    self%AAinv(3,2) = DSIN(self%theta)*DCOS(self%psi)
    self%AAinv(3,3) = DCOS(self%theta)

END SUBROUTINE initproblem

SUBROUTINE destroy(self)

    CLASS(Problem) :: self
    
    IF (ALLOCATED(self%left)) THEN
        DEALLOCATE(self%left,self%right)
    END IF
    
    RETURN

END SUBROUTINE destroy

!### the routine called to initialize every Patch ###
SUBROUTINE patch_init(self, workp, rankloc, simunits, is_boundary, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS), INTENT(OUT) :: ierr

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%init_routine))

        !### Run until t=0.08
        CASE ('Test_RJ95_1a')

            CALL self%test_RJ95_1a(simunits, tid)
            ierr = 0

        !### Run until t=0.03
        CASE ('Test_RJ95_1b')

            CALL self%test_RJ95_1b(simunits, tid)
            ierr = 0

        !### Run until t=0.2
        CASE ('Test_RJ95_2a')

            CALL self%test_RJ95_2a(simunits, tid)
            ierr = 0

        !### Run until t=0.035
        CASE ('Test_RJ95_2b')

            CALL self%test_RJ95_2b(simunits, tid)
            ierr = 0

        !### Run until t=0.01
        CASE ('Test_RJ95_3a')

            CALL self%test_RJ95_3a(simunits, tid)
            ierr = 0

        !### Run until t=0.1
        CASE ('Test_RJ95_3b')

            CALL self%test_RJ95_3b(simunits, tid)
            ierr = 0

        !### Run until t=0.15
        CASE ('Test_RJ95_4a')

            CALL self%test_RJ95_4a(simunits, tid)
            ierr = 0

        !### Run until t=0.15
        CASE ('Test_RJ95_4b')

            CALL self%test_RJ95_4b(simunits, tid)
            ierr = 0

        !### Run until t=0.15
        CASE ('Test_RJ95_4c')

            CALL self%test_RJ95_4c(simunits, tid)
            ierr = 0

        !### Run until t=0.16
        CASE ('Test_RJ95_4d')

            CALL self%test_RJ95_4d(simunits, tid)
            ierr = 0

        !### Run until t=0.1
        CASE ('Test_RJ95_5a')

            CALL self%test_RJ95_5a(simunits, tid)
            ierr = 0

        !### Run until t=0.16
        CASE ('Test_RJ95_5b')

            CALL self%test_RJ95_5b(simunits, tid)
            ierr = 0

        CASE DEFAULT

            CALL Error('Mod_Problem', 'Unknown initialization routine requested => "' // TRIM(self%init_routine) // '"')
            ierr = 1

    END SELECT

    CALL self%init_tube_states(workp, tid)


END SUBROUTINE patch_init

!### the routine called to set boundary values (called for every Patch even if it is not a world boundary) ###
SUBROUTINE patch_bounds(self, workp, rankloc, simunits, is_boundary, maxsignal, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    REAL(WRD), INTENT(INOUT) :: maxsignal
    INTEGER(WIS), INTENT(OUT) :: ierr
    INTEGER(WIS), INTENT(IN) :: tid

    maxsignal = MAX(maxsignal, 0.0_WRD)

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%bounds_routine))

       CASE('ContinuousBounds')

           CALL self%open_bounds(workp, is_boundary)

           ierr = 0

       CASE DEFAULT

           CALL Error('Mod_Problem', 'Unknown boundary routine requested => "' // TRIM(self%init_routine) // '"')
           ierr = 1
            
    END SELECT

END SUBROUTINE patch_bounds

!### RJ95 1a left and right states
SUBROUTINE test_RJ95_1a(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 1.0_WRD
    self%left(2,tid) = 10.0_WRD
    self%left(3,tid) = 0.0_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 5.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(6,tid) = 5.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 20.0_WRD

    !### right ###
    self%right(1,tid) = 1.0_WRD
    self%right(2,tid) =-10.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 5.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(6,tid) = 5.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 1.0_WRD

    RETURN

END SUBROUTINE test_RJ95_1a

!### RJ95 1b left and right states
SUBROUTINE test_RJ95_1b(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 1.0_WRD
    self%left(2,tid) = 0.0_WRD
    self%left(3,tid) = 0.0_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 3.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(6,tid) = 5.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 1.0_WRD

    !### right ###
    self%right(1,tid) = 0.1_WRD
    self%right(2,tid) = 0.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 3.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(6,tid) = 2.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 10.0_WRD

    RETURN

END SUBROUTINE test_RJ95_1b

!### RJ95 2a left and right states
SUBROUTINE test_RJ95_2a(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 1.08_WRD
    self%left(2,tid) = 1.2_WRD
    self%left(3,tid) = 0.01_WRD
    self%left(4,tid) = 0.5_WRD
    self%left(5,tid) = 2.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(6,tid) = 3.6_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(7,tid) = 2.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(8,tid) = 0.95_WRD


    !### right ###
    self%right(1,tid) = 1.0_WRD
    self%right(2,tid) = 0.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 2.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(6,tid) = 4.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(7,tid) = 2.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(8,tid) = 1.0_WRD

    RETURN

END SUBROUTINE test_RJ95_2a

!### RJ95 2b left and right states
SUBROUTINE test_RJ95_2b(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 1.0_WRD
    self%left(2,tid) = 0.0_WRD
    self%left(3,tid) = 0.0_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 3.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(6,tid) = 6.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 1.0_WRD

    !### right ###
    self%right(1,tid) = 0.1_WRD
    self%right(2,tid) = 0.0_WRD
    self%right(3,tid) = 2.0_WRD
    self%right(4,tid) = 1.0_WRD
    self%right(5,tid) = 3.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(6,tid) = 1.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 10.0_WRD

END SUBROUTINE test_RJ95_2b

!### RJ95 3a left and right states
SUBROUTINE test_RJ95_3a(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 0.1_WRD
    self%left(2,tid) = 50.0_WRD
    self%left(3,tid) = 0.0_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 0.0_WRD
    self%left(6,tid) =-1.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(7,tid) =-2.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%left(8,tid) = 0.4_WRD

    !### right ###
    self%right(1,tid) = 0.1_WRD
    self%right(2,tid) = 0.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 0.0_WRD
    self%right(6,tid) = 1.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(7,tid) = 2.0_WRD / SQRT(4.0_WRD * simunits%pi)
    self%right(8,tid) = 0.2_WRD
 
    RETURN

END SUBROUTINE test_RJ95_3a

!### RJ95 3b left and right states
SUBROUTINE test_RJ95_3b(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 1.0_WRD
    self%left(2,tid) =-1.0_WRD
    self%left(3,tid) = 0.0_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 0.0_WRD
    self%left(6,tid) = 1.0_WRD
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 1.0_WRD

    !### right ###
    self%right(1,tid) = 1.0_WRD
    self%right(2,tid) = 1.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 0.0_WRD
    self%right(6,tid) = 1.0_WRD
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 1.0_WRD
 
    RETURN

END SUBROUTINE test_RJ95_3b

!### RJ95 4a left and right states
SUBROUTINE test_RJ95_4a(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 1.0_WRD
    self%left(2,tid) = 0.0_WRD
    self%left(3,tid) = 0.0_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 1.0_WRD
    self%left(6,tid) = 1.0_WRD
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 1.0_WRD

    !### right ###
    self%right(1,tid) = 0.2_WRD
    self%right(2,tid) = 0.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 1.0_WRD
    self%right(6,tid) = 0.0_WRD
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 0.1_WRD
 
    RETURN

END SUBROUTINE test_RJ95_4a

!### RJ95 4b left and right states
SUBROUTINE test_RJ95_4b(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 0.4_WRD
    self%left(2,tid) =-0.66991_WRD
    self%left(3,tid) = 0.98263_WRD
    self%left(4,tid) = 0.0025293_WRD
    self%left(5,tid) = 1.3_WRD
    self%left(6,tid) = 0.0_WRD
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 0.52467_WRD

    !### right ###
    self%right(1,tid) = 1.0_WRD
    self%right(2,tid) = 0.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 1.3_WRD
    self%right(6,tid) = 1.0_WRD
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 1.0_WRD
 
    RETURN

END SUBROUTINE test_RJ95_4b

!### RJ95 4c left and right states
SUBROUTINE test_RJ95_4c(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 0.65_WRD
    self%left(2,tid) = 0.667_WRD
    self%left(3,tid) =-0.257_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 0.75_WRD
    self%left(6,tid) = 0.55_WRD
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 0.5_WRD

    !### right ###
    self%right(1,tid) = 1.0_WRD
    self%right(2,tid) = 0.4_WRD
    self%right(3,tid) =-0.94_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 0.75_WRD
    self%right(6,tid) = 0.0_WRD
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 0.75_WRD
 
    RETURN

END SUBROUTINE test_RJ95_4c

!### RJ95 4d left and right states
SUBROUTINE test_RJ95_4d(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 1.0_WRD
    self%left(2,tid) = 0.0_WRD
    self%left(3,tid) = 0.0_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 0.7_WRD
    self%left(6,tid) = 0.0_WRD
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 1.0_WRD

    !### right ###
    self%right(1,tid) = 0.3_WRD
    self%right(2,tid) = 0.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 1.0_WRD
    self%right(5,tid) = 0.7_WRD
    self%right(6,tid) = 1.0_WRD
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 0.2_WRD
 
    RETURN

END SUBROUTINE test_RJ95_4d

!### RJ95 5a left and right states
SUBROUTINE test_RJ95_5a(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 1.0_WRD
    self%left(2,tid) = 0.0_WRD
    self%left(3,tid) = 0.0_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 0.75_WRD
    self%left(6,tid) = 1.0_WRD
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 1.0_WRD

    !### right ###
    self%right(1,tid) = 0.125_WRD
    self%right(2,tid) = 0.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 0.75_WRD
    self%right(6,tid) =-1.0_WRD
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 0.1_WRD
 
    RETURN

END SUBROUTINE test_RJ95_5a

!### RJ95 5b left and right states
SUBROUTINE test_RJ95_5b(self, simunits, tid)

    CLASS(Problem) :: self
    TYPE(SimulationUnits) :: simunits
    INTEGER(WIS), INTENT(IN) :: tid

    !### left ###
    self%left(1,tid) = 1.0_WRD
    self%left(2,tid) = 0.0_WRD
    self%left(3,tid) = 0.0_WRD
    self%left(4,tid) = 0.0_WRD
    self%left(5,tid) = 1.3_WRD
    self%left(6,tid) = 1.0_WRD
    self%left(7,tid) = 0.0_WRD
    self%left(8,tid) = 1.0_WRD

    !### right ###
    self%right(1,tid) = 0.4_WRD
    self%right(2,tid) = 0.0_WRD
    self%right(3,tid) = 0.0_WRD
    self%right(4,tid) = 0.0_WRD
    self%right(5,tid) = 1.3_WRD
    self%right(6,tid) =-1.0_WRD
    self%right(7,tid) = 0.0_WRD
    self%right(8,tid) = 0.4_WRD
 
    RETURN

END SUBROUTINE test_RJ95_5b

SUBROUTINE init_tube_states(self, workp, tid)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    INTEGER(WIS), INTENT(IN) :: tid
    REAL(WRD) :: pressure, vl(3), vr(3), bl(3), br(3), pos(3), posf(3,3)
    INTEGER(WIS) :: i, j, k

    IF(workp%is1d) THEN

       !### loop through the grid along X only ###
       DO i = 1-workp%nb, workp%nx+workp%nb
   
           !### left hand states (along x) ###
           IF ((workp%x0 + (i-1)*workp%dx) .LE. 0.0_WRD) THEN
   
               workp%grid1d(i,1) = self%left(1,tid)
               workp%grid1d(i,2) = workp%grid1d(i,1) * self%left(2,tid)
               workp%grid1d(i,3) = workp%grid1d(i,1) * self%left(3,tid)
               workp%grid1d(i,4) = workp%grid1d(i,1) * self%left(4,tid)
               workp%grid1d(i,5) = self%left(5,tid)
               workp%grid1d(i,6) = self%left(6,tid)
               workp%grid1d(i,7) = self%left(7,tid)
               pressure          = self%left(8,tid)
               IF (workp%passOn .AND. workp%npass .GT. 0) workp%pass1d(i,:) = 0.0_WRD
   
           !### right hand states (along x) ###
           ELSE
   
               workp%grid1d(i,1) = self%right(1,tid)
               workp%grid1d(i,2) = workp%grid1d(i,1) * self%right(2,tid)
               workp%grid1d(i,3) = workp%grid1d(i,1) * self%right(3,tid)
               workp%grid1d(i,4) = workp%grid1d(i,1) * self%right(4,tid)
               workp%grid1d(i,5) = self%right(5,tid)
               workp%grid1d(i,6) = self%right(6,tid)
               workp%grid1d(i,7) = self%right(7,tid)
               pressure          = self%right(8,tid)
               IF (workp%passOn .AND. workp%npass .GT. 0) workp%pass1d(i,:) = 1.0_WRD
   
           END IF
   
           !### now set energy ###
           workp%grid1d(i,8) = pressure / (self%gamma - 1.0_WRD) + (0.5_WRD / workp%grid1d(i,1)) * (workp%grid1d(i,2)**2 + workp%grid1d(i,3)**2 + workp%grid1d(i,4)**2) + &
                               0.5_WRD * (workp%grid1d(i,5)**2 + workp%grid1d(i,6)**2 + workp%grid1d(i,7)**2)
   
       END DO

    ELSE IF(workp%is2d) THEN

       !### do the rotation ###
       vl = (/ self%left(2,tid) , self%left(3,tid) , self%left(4,tid)  /)
       bl = (/ self%left(5,tid) , self%left(6,tid) , self%left(7,tid)  /)
       vr = (/ self%right(2,tid), self%right(3,tid), self%right(4,tid) /)
       br = (/ self%right(5,tid), self%right(6,tid), self%right(7,tid) /)       

       CALL self%rotate2d(vl(1:2), self%A, self%Ainv, .FALSE.)
       CALL self%rotate2d(bl(1:2), self%A, self%Ainv, .FALSE.)
       CALL self%rotate2d(vr(1:2), self%A, self%Ainv, .FALSE.)
       CALL self%rotate2d(br(1:2), self%A, self%Ainv, .FALSE.)

       !### vary though the grid along X only ###
       DO j = 1-workp%nb, workp%ny+workp%nb
   
           DO i = 1-workp%nb, workp%nx+workp%nb
   
               pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
               pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
               posf(1,1) = pos(1) + 0.5_WRD*workp%dx
               posf(2,1) = pos(2)
               posf(1,2) = pos(1)
               posf(2,2) = pos(2) + 0.5_WRD*workp%dx
               CALL self%rotate2d(pos, self%A, self%Ainv, .TRUE.)
               CALL self%rotate2d(posf(:,1), self%A, self%Ainv, .TRUE.)
               CALL self%rotate2d(posf(:,2), self%A, self%Ainv, .TRUE.)

               !### left hand states (along x) ###
               IF (pos(1) .LE. 1.0E-14_WRD) THEN
   
                   workp%grid2d(i,j,1)  = self%left(1,tid)
                   workp%grid2d(i,j,2)  = workp%grid2d(i,j,1) * vl(1)
                   workp%grid2d(i,j,3)  = workp%grid2d(i,j,1) * vl(2)
                   workp%grid2d(i,j,4)  = workp%grid2d(i,j,1) * vl(3)
                   workp%grid2d(i,j,7)  = self%left(7,tid)
                   IF (workp%passOn .AND. workp%npass .GT. 0) workp%pass2d(i,j,:) = 0.0_WRD
                 
               !### right hand states (along x) ###
               ELSE
                   
                   workp%grid2d(i,j,1)  = self%right(1,tid)
                   workp%grid2d(i,j,2)  = workp%grid2d(i,j,1) * vr(1)
                   workp%grid2d(i,j,3)  = workp%grid2d(i,j,1) * vr(2)
                   workp%grid2d(i,j,4)  = workp%grid2d(i,j,1) * vr(3)
                   workp%grid2d(i,j,7)  = self%right(7,tid)
                   IF (workp%passOn .AND. workp%npass .GT. 0) workp%pass2d(i,j,:) = 1.0_WRD
                   
               END IF
   
               IF(posf(1,1) .LE. 1.0E-14_WRD) THEN
                   workp%bface2d(i,j,1) = bl(1)
               ELSE
                   workp%bface2d(i,j,1) = br(1)
               END IF

               IF (posf(1,2) .LE. 1.0E-14_WRD) THEN
                   workp%bface2d(i,j,2) = bl(2)
               ELSE
                   workp%bface2d(i,j,2) = br(2)
               END IF
   
           END DO
   
       END DO
   
       !### loop back through and set Bx/y and energy ###
       DO j = 1, workp%ny
   
           DO i = 1, workp%nx
   
               pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
               pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
               CALL self%rotate2d(pos, self%A, self%Ainv, .TRUE.)

               IF (pos(1) .LE. 1.0E-14_WRD) THEN
                   pressure = self%left(8,tid)
               ELSE
                   pressure = self%right(8,tid)
               END IF
               workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i-1,j,1) + workp%bface2d(i,j,1))
               workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j-1,2) + workp%bface2d(i,j,2))
               workp%grid2d(i,j,8) = pressure / (self%gamma - 1.0_WRD) + (0.5_WRD / workp%grid2d(i,j,1)) * (workp%grid2d(i,j,2)**2 + workp%grid2d(i,j,3)**2 + workp%grid2d(i,j,4)**2) + &
                    0.5_WRD * (workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2 + workp%grid2d(i,j,7)**2)
          END DO 

       END DO

    ELSE IF(workp%is3d) THEN

       !### do the rotation ###
       vl = (/ self%left(2,tid) , self%left(3,tid) , self%left(4,tid)  /)
       bl = (/ self%left(5,tid) , self%left(6,tid) , self%left(7,tid)  /)
       vr = (/ self%right(2,tid), self%right(3,tid), self%right(4,tid) /)
       br = (/ self%right(5,tid), self%right(6,tid), self%right(7,tid) /)       

       CALL self%rotate3d(vl, self%AA, self%AAinv, .FALSE.)
       CALL self%rotate3d(bl, self%AA, self%AAinv, .FALSE.)
       CALL self%rotate3d(vr, self%AA, self%AAinv, .FALSE.)
       CALL self%rotate3d(br, self%AA, self%AAinv, .FALSE.)     

       DO k = 1-workp%nb, workp%nz+workp%nb
   
           DO j = 1-workp%nb, workp%ny+workp%nb
   
               DO i = 1-workp%nb, workp%nx+workp%nb

                  pos(1)    = workp%x0 + REAL((i-1),WRD)*workp%dx
                  pos(2)    = workp%y0 + REAL((j-1),WRD)*workp%dx
                  pos(3)    = workp%z0 + REAL((k-1),WRD)*workp%dx
                  posf(1,1) = pos(1) + 0.5_WRD*workp%dx
                  posf(2,1) = pos(2)
                  posf(3,1) = pos(3)
                  posf(1,2) = pos(1)
                  posf(2,2) = pos(2) + 0.5_WRD*workp%dx
                  posf(3,2) = pos(3)
                  posf(1,3) = pos(1)
                  posf(2,3) = pos(2)
                  posf(3,3) = pos(3) + 0.5_WRD*workp%dx
                  CALL self%rotate3d(pos, self%AA, self%AAinv, .TRUE.)
                  CALL self%rotate3d(posf(:,1), self%AA, self%AAinv, .TRUE.)
                  CALL self%rotate3d(posf(:,2), self%AA, self%AAinv, .TRUE.)
                  CALL self%rotate3d(posf(:,3), self%AA, self%AAinv, .TRUE.)

                  !### left hand states (along x) ###
                  IF (pos(1) .LE. 1.0E-14_WRD) THEN

                      workp%grid3d(i,j,k,1)  = self%left(1,tid)
                      workp%grid3d(i,j,k,2)  = workp%grid3d(i,j,k,1) * vl(1)
                      workp%grid3d(i,j,k,3)  = workp%grid3d(i,j,k,1) * vl(2)
                      workp%grid3d(i,j,k,4)  = workp%grid3d(i,j,k,1) * vl(3)
                      IF (workp%passOn .AND. workp%npass .GT. 0) workp%pass3d(i,j,k,:) = 0.0_WRD
                    
                  !### right hand states (along x) ###
                  ELSE
                      
                      workp%grid3d(i,j,k,1)  = self%right(1,tid)
                      workp%grid3d(i,j,k,2)  = workp%grid3d(i,j,k,1) * vr(1)
                      workp%grid3d(i,j,k,3)  = workp%grid3d(i,j,k,1) * vr(2)
                      workp%grid3d(i,j,k,4)  = workp%grid3d(i,j,k,1) * vr(3)
                      IF (workp%passOn .AND. workp%npass .GT. 0) workp%pass3d(i,j,k,:) = 1.0_WRD

                  END IF


                  IF(posf(1,1) .LE. 1.0E-14_WRD) THEN
                      workp%bface3d(i,j,k,1) = bl(1)
                  ELSE
                      workp%bface3d(i,j,k,1) = br(1)
                  END IF
   
                  IF (posf(1,2) .LE. 1.0E-14_WRD) THEN
                      workp%bface3d(i,j,k,2) = bl(2)
                  ELSE
                      workp%bface3d(i,j,k,2) = br(2)
                  END IF

                  IF (posf(1,3) .LE. 1.0E-14_WRD) THEN
                      workp%bface3d(i,j,k,3) = bl(3)
                  ELSE
                      workp%bface3d(i,j,k,3) = br(3)
                  END IF

               END DO

           END DO

       END DO

       !### loop back through and set grid Bs and energy ###
       DO k = 1, workp%nz
   
           DO j = 1, workp%ny
   
               DO i = 1, workp%nx
   
                  pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
                  pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx
                  pos(3) = workp%z0 + REAL((k-1),WRD)*workp%dx
                  CALL self%rotate3d(pos, self%AA, self%AAinv, .TRUE.)

                  IF (pos(1) .LE. 1.0E-14_WRD) THEN
                      pressure = self%left(8,tid)
                  ELSE
                      pressure = self%right(8,tid)
                  END IF
                  workp%grid3d(i,j,k,5) = 0.5_WRD * (workp%bface3d(i-1,j,k,1) + workp%bface3d(i,j,k,1))
                  workp%grid3d(i,j,k,6) = 0.5_WRD * (workp%bface3d(i,j-1,k,2) + workp%bface3d(i,j,k,2))
                  workp%grid3d(i,j,k,7) = 0.5_WRD * (workp%bface3d(i,j,k-1,3) + workp%bface3d(i,j,k,3))
                  workp%grid3d(i,j,k,8) = pressure / (self%gamma - 1.0_WRD) + (0.5_WRD / workp%grid3d(i,j,k,1)) * (workp%grid3d(i,j,k,2)**2 + workp%grid3d(i,j,k,3)**2 + workp%grid3d(i,j,k,4)**2) + &
                       0.5_WRD * (workp%grid3d(i,j,k,5)**2 + workp%grid3d(i,j,k,6)**2 + workp%grid3d(i,j,k,7)**2)
   
               END DO
               
           END DO
   
       END DO

    END IF

    RETURN

END SUBROUTINE init_tube_states

END MODULE Mod_Problem
