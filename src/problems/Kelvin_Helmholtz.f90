#include "../config.h"

MODULE Mod_Problem

!######################################################################
!#
!# FILENAME: mod_KelvinHelmholtz_tests.f90
!#
!# DESCRIPTION: This module provides a class for initializing a domain
!#   and setting boundary values for a 2d KH sim.
!#
!# DEPENDENCIES: Mod_Globals for defining data types
!#
!# HISTORY:
!#    9/14/2015  - Pete Mendygral
!#
!######################################################################

!### require the needed modules ###
USE Mod_Globals
USE Mod_Error
USE Mod_ProblemBase
USE Mod_RankLocation
USE Mod_SimulationUnits
USE Mod_Patch

IMPLICIT NONE

!### default private but make the data type available ###
PRIVATE
PUBLIC :: Problem

!### define a data type for this module ###
TYPE, EXTENDS(ProblemBase) :: Problem

    !### default everything as private ###
    PRIVATE

    CHARACTER*30 :: init_routine, bounds_routine
    REAL(WRD), PUBLIC :: gamma, amplitude, pressure, a, sigma, z1, z2, ux, density_ratio

    CONTAINS

    !### default everything as private ###
    PRIVATE

    PROCEDURE, PUBLIC :: destroy
    PROCEDURE, PUBLIC :: patch_init
    PROCEDURE, PUBLIC :: patch_bounds
    PROCEDURE :: initproblem
    PROCEDURE :: kh_init

END TYPE Problem

!### create an interface to the constructor ###
INTERFACE Problem

    MODULE PROCEDURE constructor

END INTERFACE Problem

!### object methods ###
CONTAINS

!### constructor for the class ###
!###   arguments are: the namelist file contents for grabbing parameters out of ###
FUNCTION constructor(namelist, nthreads, rankloc, simunits)

    TYPE(Problem) :: constructor
    CHARACTER*(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits

    CALL constructor%initproblem(namelist, nthreads, rankloc, simunits)
    RETURN

END FUNCTION constructor

!### this function will init the object and configure init and bound routine parameters ###
SUBROUTINE initproblem(self, namelist, nthreads, rankloc, simunits)

    CLASS(Problem) :: self
    CHARACTER*(WMAX_NAMELIST_LEN), INTENT(IN) :: namelist
    INTEGER(WIS), INTENT(IN) :: nthreads
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    CHARACTER*30 :: init_routine, bounds_routine
    REAL(WRD) :: gamma, amplitude, pressure, a, sigma, z1, z2, ux, density_ratio

    NAMELIST /ProblemSetup/ init_routine, bounds_routine, gamma, amplitude, pressure, a, sigma, z1, z2, ux, density_ratio

    !### set the routines we'll call ###
    READ(namelist, NML=ProblemSetup)
    self%init_routine   = init_routine
    self%bounds_routine = bounds_routine

    !### we handle setting the adiabatic index ###
    self%gamma         = gamma
    self%amplitude     = amplitude
    self%pressure      = pressure
    self%a             = a
    self%sigma         = sigma
    self%z1            = z1
    self%z2            = z2
    self%ux            = ux
    self%density_ratio = density_ratio

END SUBROUTINE initproblem

SUBROUTINE destroy(self)

    CLASS(Problem) :: self
    
    RETURN

END SUBROUTINE destroy

!### the routine called to initialize every Patch ###
SUBROUTINE patch_init(self, workp, rankloc, simunits, is_boundary, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    INTEGER(WIS), INTENT(IN) :: tid
    INTEGER(WIS), INTENT(OUT) :: ierr

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%init_routine))

        CASE ('KH')

            CALL self%kh_init(workp, simunits)
            ierr = 0

        CASE DEFAULT

            CALL Error('Mod_Problem', 'Unknown initialization routine requested => "' // TRIM(self%init_routine) // '"')
            ierr = 1

    END SELECT

END SUBROUTINE patch_init

!### the routine called to set boundary values (called for every Patch even if it is not a world boundary) ###
SUBROUTINE patch_bounds(self, workp, rankloc, simunits, is_boundary, maxsignal, tid, ierr)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(RankLocation) :: rankloc
    TYPE(SimulationUnits) :: simunits
    LOGICAL(WIS), INTENT(IN) :: is_boundary(2,3)
    REAL(WRD), INTENT(INOUT) :: maxsignal
    INTEGER(WIS), INTENT(OUT) :: ierr
    INTEGER(WIS), INTENT(IN) :: tid

    maxsignal = MAX(maxsignal, 0.0_WRD)

    !### route to the correct initialization routine ###
    SELECT CASE (TRIM(self%bounds_routine))

       CASE DEFAULT

           !### we're periodic in all directions so do nothing ###
           ierr = 0

    END SELECT

END SUBROUTINE patch_bounds

!### KH setup ###
SUBROUTINE kh_init(self, workp, simunits)

    CLASS(Problem) :: self
    TYPE(Patch) :: workp
    TYPE(SimulationUnits) :: simunits
    REAL(WRD) :: pos(2)
    INTEGER(WIS) :: i, j, k

    IF(workp%is2d) THEN

       !### vary though the grid along X only ###
       DO j = 1-workp%nb, workp%ny+workp%nb
   
           DO i = 1-workp%nb, workp%nx+workp%nb
   
               pos(1) = workp%x0 + REAL((i-1),WRD)*workp%dx
               pos(2) = workp%y0 + REAL((j-1),WRD)*workp%dx

               workp%grid2d(i,j,1)  = 1.0_WRD + self%density_ratio * 0.5_WRD * (TANH((pos(2) - self%z1)/ self%a) - TANH((pos(2) - self%z2) / self%a))
               workp%grid2d(i,j,2)  = workp%grid2d(i,j,1) * self%ux * (TANH((pos(2) - self%z1) / self%a) - TANH((pos(2) - self%z2) / self%a) - 1.0_WRD)
               workp%grid2d(i,j,3)  = workp%grid2d(i,j,1) * self%amplitude * SIN(2.0_WRD * simunits%pi * pos(1)) * (EXP(-(pos(2) - self%z1)**2 / self%sigma**2) + EXP(-(pos(2) - self%z2)**2 / self%sigma**2))
               workp%grid2d(i,j,4)  = 0.0_WRD
               workp%bface2d(i,j,1) = 0.0_WRD
               workp%bface2d(i,j,2) = 0.0_WRD
               workp%grid2d(i,j,7)  = 0.0_WRD
               IF (workp%passOn .AND. workp%npass .GT. 0) THEN

                   workp%pass2d(i,j,:) = 0.5_WRD * (TANH((pos(2) - self%z2) / self%a) - TANH((pos(2) - self%z1) / self%a) + 2.0_WRD)

               END IF
   
           END DO
   
       END DO
   
       !### loop back through and set Bx/y and energy ###
       DO j = 1, workp%ny
   
           DO i = 1, workp%nx

               workp%grid2d(i,j,5) = 0.5_WRD * (workp%bface2d(i,j,1) + workp%bface2d(i-1,j,1))
               workp%grid2d(i,j,6) = 0.5_WRD * (workp%bface2d(i,j,2) + workp%bface2d(i,j-1,2))

               workp%grid2d(i,j,8) = self%pressure / (self%gamma - 1.0_WRD) + (0.5_WRD / workp%grid2d(i,j,1)) * (workp%grid2d(i,j,2)**2 + workp%grid2d(i,j,3)**2 + workp%grid2d(i,j,4)**2) + &
                    0.5_WRD * (workp%grid2d(i,j,5)**2 + workp%grid2d(i,j,6)**2 + workp%grid2d(i,j,7)**2)
           END DO

       END DO

    END IF

END SUBROUTINE kh_init

END MODULE Mod_Problem
