 PROGRAM fishie

        IMPLICIT NONE

        TYPE :: level

                REAL*8, ALLOCATABLE :: grid(:,:,:,:), source(:,:,:,:)
                REAL*8 :: dx
                INTEGER :: nx, ny, nz

        END TYPE

        TYPE :: MGM

                TYPE(level), ALLOCATABLE :: levelo(:)
                REAL*8 :: maxresidual
                INTEGER :: nlevels, nsmooth
                LOGICAL :: periodic

        END TYPE


        TYPE(MGM) :: mgmo
        INTEGER :: nx, ny, nz, nsmooth, nlevels, i,j,k
        REAL*8 :: lx, dx, maxresidual
        REAL*8, ALLOCATABLE :: source(:,:,:)
        LOGICAL :: periodic 

        !### set the problem parameters ###
        lx = 1.d0
        nx = 10
        ny = 10
        nz = 10
        nsmooth = 20
        nlevels = 2
        periodic = .TRUE.
        maxresidual = 1.d-14

        !### set the size of a voxel, and the even and odd voxel
        !starting and ending places
        dx = lx/nx

        !### make space for sources ###
        ALLOCATE(source(nx,ny,nz))

        !### fill in the source
        CALL SourceInitialize(source,nx,ny,nz)

        CALL MGM_Init(mgmo,source,nx,ny,nz,nsmooth,nlevels,dx,maxresidual,periodic)

        OPEN(24,FILE="source_xcut.data")

        DO i = 1, nx

                WRITE(24,"(2F15.2)") i*dx,mgmo%levelo(nlevels)%source(1,i,ny/2,nz/2)

        END DO

        CLOSE(24)

        OPEN(24,FILE="source_ycut.data")

        DO j = 1, ny

                WRITE(24,"(2F15.2)") j*dx, mgmo%levelo(nlevels)%source(1,nx/2,j,nz/2)

        END DO

        CLOSE(24)

        OPEN(24,FILE="source_zcut.data")

        DO k = 1, nz

                WRITE(24,"(2F15.2)") k*dx, mgmo%levelo(nlevels)%source(1,nx/2,ny/2,k)

        END DO

        CLOSE(24)


        PRINT*, "mgm_init ended, begin MBM_vcycle"

        CALL MGM_Vcycle(mgmo%levelo(mgmo%nlevels),mgmo%levelo(mgmo%nlevels-1),mgmo%nsmooth,mgmo%maxresidual, mgmo%periodic)
         
        OPEN(24,FILE="poisson_xcut.data")

        DO i = 1, nx

                WRITE(24,"(2F15.5)") i*dx, mgmo%levelo(nlevels)%grid(1,i,ny/2,nz/2)

        END DO

        CLOSE(24)

        OPEN(24,FILE="poisson_ycut.data")

        DO j = 1, ny

                WRITE(24,"(2F15.5)") j*dx, mgmo%levelo(nlevels)%grid(1,nx/2,j,nz/2)

        END DO

        CLOSE(24)

        OPEN(24,FILE="poisson_zcut.data")

        DO k = 1, nz

                WRITE(24,"(2F15.5)") k*dx, mgmo%levelo(nlevels)%grid(1,nx/2,ny/2,k)

        END DO

        CLOSE(24)


        
        CONTAINS

        SUBROUTINE MGM_Init(self,source,nx,ny,nz,nsmooth,nlevels,dx,maxresidual, periodic)
                
        TYPE(MGM), INTENT(INOUT) :: self
        REAL*8, INTENT(IN) :: source(:,:,:), dx, maxresidual
        INTEGER, INTENT(IN) :: nx, ny, nz, nsmooth, nlevels
        LOGICAL, INTENT(IN) :: periodic
        REAL*8 :: nf
        INTEGER :: n

        !### pass mgm info ###
        self%nlevels     = nlevels
        self%nsmooth     = nsmooth
        self%maxresidual = maxresidual
        self%periodic     = periodic

        ALLOCATE(self%levelo(nlevels))   


        PRINT*,"TESTING nx,ny,nz for compatibility with 2^n-1 scheme.", nlevels, "requires a number divisible by" , 2**(nlevels-1)
        IF (MOD(nx,2**(nlevels-1)) .NE. 0 ) STOP
        IF (MOD(ny,2**(nlevels-1)) .NE. 0 ) STOP
        IF (MOD(nz,2**(nlevels-1)) .NE. 0 ) STOP     

        !### loop through the levels to initialize each ###
        DO n = nlevels, 1, -1

                !### factor by which quantities are changed between levels ###
                nf = 2.d0**(nlevels - n)
                self%levelo(n)%dx = dx*nf
                self%levelo(n)%nx = nx/nf
                self%levelo(n)%ny = ny/nf
                self%levelo(n)%nz = nz/nf

                !### allocate grid space for the levels ###
                ALLOCATE(self%levelo(n)%grid(2,self%levelo(n)%nx,self%levelo(n)%ny,self%levelo(n)%nz))
                ALLOCATE(self%levelo(n)%source(2,self%levelo(n)%nx,self%levelo(n)%ny,self%levelo(n)%nz))

                self%levelo(n)%grid      = 0.d0
                self%levelo(n)%source   = 0.d0

        END DO 

        !### fill in the fine grid source ###
        self%levelo(nlevels)%source(1,:,:,:) = source(:,:,:)


        END SUBROUTINE MGM_Init

        SUBROUTINE MGM_Destroy(self)
        TYPE(MGM), INTENT(INOUT) :: self
        INTEGER :: n

        DO n = 1, self%nlevels

                DEALLOCATE(self%levelo(n)%grid)
                DEALLOCATE(self%levelo(n)%source)

        END DO

        DEALLOCATE(self%levelo)

        END SUBROUTINE MGM_Destroy


        SUBROUTINE MGM_Vcycle(flevelo, clevelo,nsmooth,maxresidual,periodic)
        TYPE(level), INTENT(INOUT) :: flevelo, clevelo
        REAL*8, INTENT(IN) ::maxresidual
        INTEGER, INTENT(IN) :: nsmooth
        LOGICAL, INTENT(IN) :: periodic
        REAL*8 :: currentmaxresidual
        INTEGER :: n


        currentmaxresidual = 1.d8

                !### presmooth
                DO n = 1, nsmooth

                        CALL MGM_smooth(flevelo%grid(1,:,:,:),flevelo%source(1,:,:,:),flevelo%nx,flevelo%ny,flevelo%nz, flevelo%dx, periodic)      
 
                END DO

                PRINT*, "end presmooth, begin residual"

                !### residual ###
                CALL MGM_residual(flevelo%source,flevelo%grid(1,:,:,:),flevelo%nx,flevelo%ny,flevelo%nz,flevelo%dx)


                PRINT*, "end residual, begin coarsen"


                !### coarsen residual ###
                CALL MGM_Coarsen(flevelo%source(2,:,:,:),clevelo%source(1,:,:,:), flevelo%nx, flevelo%ny, flevelo%nz)


                PRINT*, "end coarsen, begin max residual find"

                n = 1

                PRINT*, currentmaxresidual, maxresidual

                DO WHILE (ABS(currentmaxresidual) .GT. ABS(maxresidual) .AND. n .LT. 1000)

                        !### smooth on errori ###
                        CALL MGM_Smooth(clevelo%grid(1,:,:,:),clevelo%source(1,:,:,:),clevelo%nx, clevelo%ny, clevelo%nz, clevelo%dx, periodic)

                        CALL MGM_residual(clevelo%source,clevelo%grid(1,:,:,:),clevelo%nx,clevelo%ny,clevelo%nz,clevelo%dx)

                        currentmaxresidual = MAXVAL(ABS(clevelo%source(2,:,:,:)))

                        n = n + 1

                PRINT*, n, currentmaxresidual, maxresidual

                END DO

                PRINT*, "end max residual find, begin refine"

                !### refine errors ###
                CALL MGM_Refine(clevelo%grid(1,:,:,:),flevelo%grid(2,:,:,:), clevelo%nx, clevelo%ny, clevelo%nz)

                PRINT*, "end refine, begin error add"

                !add teh error back ###
                flevelo%grid(1,:,:,:) = flevelo%grid(1,:,:,:) + flevelo%grid(2,:,:,:)

        END SUBROUTINE

        SUBROUTINE MGM_smooth(grid, source, nx, ny, nz, dx, periodic)
        REAL*8, INTENT(INOUT) :: grid(:,:,:)
        REAL*8, INTENT(IN) :: source(:,:,:), dx
        REAL*8 :: sdx2
        INTEGER, INTENT(IN) :: nx, ny, nz
        LOGICAL, INTENT(IN) :: periodic
        INTEGER :: i,j,k, mod2k, mod2j, layerIndex

        !### set the boundaries ###
        !### top and bottom z ###
        DO j = 1, ny
                
                !### are we on an even or odd voxel row in y? ###
                mod2j = MOD(j,2)

                !### layerIndex lets us know if our first non-boundary voxel at i,j,k is a red or a black, 0:red, 1:black ###
                layerIndex = MOD(1+mod2j,2)
                
                !### smooth reds ###
                DO i = 1 + layerIndex, nx, 2

                        !boundary conditions
                        IF (periodic) THEN

                                grid(i,j,1) = grid(i,j,nz-1)

                        ELSE

                                grid(i,j,1) = grid(i,j,2)

                        END IF
        

                END DO

                DO i = 2 - layerIndex, nx, 2

                        !### just boundary conditions for now
                        IF (periodic) THEN

                                grid(i,j,1) = grid(i,j,nz-1)

                        ELSE
                        
                                grid(i,j,1) = grid(i,j,2)

                        END IF

                 END DO

        END DO

        DO j = 1, ny

                !### are we on an even or odd voxel row in y? ###
                mod2j = MOD(j,2)

                !### layerIndex lets us know if our first non-boundary
                !voxel at i,j,k is a red or a black, 0:red, 1:black ###
                layerIndex = MOD(1+mod2j,2)

                !### smooth reds ###
                DO i = 1 + layerIndex, nx, 2

                        IF (periodic) THEN

                                grid(i,j,nz) = grid(i,j,2)

                        ELSE

                                grid(i,j,nz) = grid(i,j,nz-1)

                        END IF

                END DO

                DO i = 2 - layerIndex, nx, 2

                        IF (periodic) THEN

                                grid(i,j,nz) = grid(i,j,2)

                        ELSE

                                grid(i,j,nz) = grid(i,j,nz-1)

                        END IF

                END DO

        END DO

        !### do top and bottom y ###
        DO k = 1, nz
                
                !### are we on an even or odd voxel row in y? ###
                mod2k = MOD(k,2)

                !### layerIndex lets us know if our first non-boundary voxel at i,j,k is a red or a black, 0:red, 1:black ###
                layerIndex = MOD(1+mod2k,2)
                
                !### smooth reds ###
                DO i = 1 + layerIndex, nx, 2

                        IF (periodic) THEN

                                grid(i,1,k) = grid(i,ny-1,k)

                        ELSE

                                grid(i,1,k) = grid(i,2,k)

                        END IF


                END DO

                DO i = 2 - layerIndex, nx, 2

                        IF (periodic) THEN

                                grid(i,1,k) = grid(i,ny-1,k)

                        ELSE

                                grid(i,1,k) = grid(i,2,k)

                        END IF

                END DO

        END DO

        DO k = 1, nz

                !### are we on an even or odd voxel row in y? ###
                mod2k = MOD(k,2)

                !### layerIndex lets us know if our first non-boundary
                !voxel at i,j,k is a red or a black, 0:red, 1:black ###
                layerIndex = MOD(1+mod2k,2)

                !### smooth reds ###
                DO i = 1 + layerIndex, nx, 2
        
                        IF (periodic) THEN

                                grid(i,ny,k) = grid(i,2,k)

                        ELSE

                                grid(i,ny,k) = grid(i,ny-1,k)

                        END IF

                END DO

                DO i = 2 - layerIndex, nx, 2
                        
                        IF (periodic) THEN

                                grid(i,ny,k) = grid(i,2,k)

                        ELSE

                                grid(i,ny,k) = grid(i,ny-1,k)
                
                        END IF


                END DO

        END DO

        !### do top and bottom x ###
        DO k = 1, nz
                
                !### are we on an even or odd voxel row in y? ###
                mod2k = MOD(k,2)

                !### layerIndex lets us know if our first non-boundary ###
                !### voxel at i,j,k is a red or a black, 0:red, 1:black ###
                layerIndex = MOD(1+mod2k,2)
                
                !### smooth reds ###
                DO j = 1 + layerIndex, ny, 2

                        IF (periodic) THEN

                                grid(1,j,k) = grid(nx-1,j,k)

                        ELSE

                                grid(1,j,k) = grid(2,j,k)
                
                        END IF

                END DO

                DO j = 2 - layerIndex, ny, 2

                        IF (periodic) THEN

                                grid(1,j,k) = grid(nx-1,j,k)

                        ELSE

                                grid(1,j,k) = grid(2,j,k)

                        END IF

                END DO

        END DO

        DO k = 1, nz

                !### are we on an even or odd voxel row in y? ###
                mod2k = MOD(k,2)

                !### layerIndex lets us know if our first non-boundary
                !voxel at i,j,k is a red or a black, 0:red, 1:black ###
                layerIndex = MOD(1+mod2k,2)

                !### smooth reds ###
                DO j = 1 + layerIndex, ny, 2

                        IF (periodic) THEN

                                grid(nx,j,k) = grid(2,j,k)

                        ELSE

                                grid(nx,j,k) = grid(nx-1,j,k)

                        END IF

                END DO

                DO j = 2 - layerIndex, ny, 2

                        IF (periodic) THEN

                                grid(nx,j,k) = grid(2,j,k)

                        ELSE

                                grid(nx,j,k) = grid(nx-1,j,k)

                        END IF

                END DO

        END DO

        !### loop over the interior of the grid 
        DO  k = 2, nz -1

                !### are we on an even or odd voxel layer in z? ###
                mod2k = MOD(k,2)

                DO  j = 2 , ny -1

                        !### are we on an even or odd voxel row in y? ###
                        mod2j = MOD(j,2)
                        
                        !### layerIndex lets us know if our first non-boundary voxel at i,j,k is a red or a black, 0:red, 1:black ###
                        layerIndex = MOD(mod2k+mod2j,2)                

                        !### smooth the reds ###
                        DO  i = 2 + layerIndex, nx -1, 2

                                sdx2 = source(i,j,k)*dx**2

                                grid(i,j,k) = poisson_solve(grid(i+1,j,k), grid(i-1,j,k), grid(i,j+1,k), grid(i,j-1,k), grid(i,j,k+1), grid(i,j,k-1), sdx2)

                        END DO

                END DO
        
        END DO


        DO  k = 2, nz -1

                !### are we on an even or odd voxel layer in z? ###
                mod2k = MOD(k,2)

                DO  j = 2 , ny -1

                        !### are we on an even or odd voxel row in y? ###
                        mod2j = MOD(j,2)
                        
                        !### layerIndex lets us know if our first non-boundary voxel at i,j,k is a red or a black, 0:red, 1:black ###
                        layerIndex = MOD(mod2k+mod2j,2)                

                        !### smooth the blacks ###
                        DO  i = 3 - layerIndex, nx -1, 2

                                sdx2 = source(i,j,k)*dx**2

                                grid(i,j,k) = poisson_solve(grid(i+1,j,k), grid(i-1,j,k), grid(i,j+1,k), grid(i,j-1,k), grid(i,j,k+1), grid(i,j,k-1), sdx2)


                        END DO
        
                END DO
        
        END DO

        END SUBROUTINE MGM_smooth

        REAL*8 FUNCTION poisson_solve(pip1, pim1, pjp1, pjm1, pkp1, pkm1, srcdx2)
        REAL*8, INTENT(IN) :: pip1, pim1, pjp1, pjm1, pkp1, pkm1,srcdx2 

               poisson_solve = 0.125d0 * (pip1 + pim1 + pjp1 + pjm1 +  pkp1 + pkm1 - srcdx2)
!                PRINT*, pip1 + pim1 + pjp1 + pjm1 +  pkp1 + pkm1, - srcdx2
               RETURN 

        END FUNCTION poisson_solve

        SUBROUTINE MGM_Residual(source,grid,nx,ny,nz,dx)
        REAL*8, INTENT(INOUT) :: source(:,:,:,:), grid(:,:,:)
        REAL*8, INTENT(IN) :: dx
        INTEGER, INTENT(IN) :: nx, ny, nz
        INTEGER :: i,j,k

        DO k = 2, nz - 1

                DO j = 2, ny - 1

                        DO i = 2, nx - 1

                                source(2,i,j,k) = (grid(i+1,j,k) + grid(i-1,j,k) + grid(i,j+1,k) + grid(i,j-1,k) &
                                        + grid(i,j,k+1) + grid(i,j,k-1) - 8.d0*grid(i,j,k))/dx**2 - source(1,i,j,k)

                                IF (j .EQ. 5 .AND. k .EQ. 5) PRINT*, i, grid(i+1,j,k) + grid(i-1,j,k) + grid(i,j+1,k) &
                                        + grid(i,j-1,k)+ grid(i,j,k+1) + grid(i,j,k-1), - 8.d0*grid(i,j,k),dx, & 
                                        source(1, i,j,k), source(2,i,j,k)

        
                        END DO

                END DO

        END DO

        END SUBROUTINE MGM_Residual

        !### coarsen the find grid to the coarse one, using the fine nx,ny and nz
        SUBROUTINE MGM_Coarsen(fgrid,cgrid,nx,ny,nz)
        REAL*8, INTENT(IN) :: fgrid(:,:,:)
        REAL*8, INTENT(INOUT) :: cgrid(:,:,:)
        INTEGER, INTENT(IN) :: nx,ny,nz
        INTEGER :: i,j,k


        DO k = 2, nz-1,2

                DO j = 2, ny-1,2

                        DO i = 2, nx-1,2

                                cgrid(i/2,j/2,k/2) = 0.125d0*fgrid(i,j,k) + &
                        0.0625d0*(fgrid(i-1,j,k) + fgrid(i+1,j,k) + fgrid(i,j-1,k) &
                        + fgrid(i,j+1,k) + fgrid(i,j,k-1) + fgrid(i,j,k+1)) + &
                        0.03125d0*(fgrid(i-1,j-1,k) + fgrid(i-1,j+1,k) + fgrid(i-1,j,k-1) &
                        + fgrid(i-1,j,k+1) + fgrid(i,j-1,k-1) + fgrid(i,j-1,k+1) + &
                        fgrid(i,j+1,k-1) + fgrid(i,j+1,k+1) + fgrid(i+1,j-1,k) + &
                        fgrid(i+1,j+1,k) + fgrid(i+1,j,k-1) + fgrid(i+1,j,k+1)) + &
                        0.015625d0*(fgrid(i-1,j-1,k-1) + fgrid(i-1,j-1,k+1) + &
                        fgrid(i-1,j+1,k-1) + fgrid(i-1,j+1,k+1) + fgrid(i+1,j-1,k-1) + &
                        fgrid(i+1,j-1,k+1) + fgrid(i+1,j+1,k-1) + fgrid(i+1,j+1,k+1))

                        IF (j .EQ. 4 .AND. k .EQ. 4) PRINT*, i,fgrid(i,j,k), cgrid(i/2,j/2,k/2)
                        
                        END DO

                END DO

        END DO

        END SUBROUTINE MGM_Coarsen

        !### find the find grid from the coarse grid using te coarse grid nx, ny, nz
        SUBROUTINE MGM_Refine(cgrid,fgrid,nx,ny,nz)
        REAL*8, INTENT(IN) :: cgrid(:,:,:)
        REAL*8, INTENT(INOUT) :: fgrid(:,:,:)
        INTEGER, INTENT(IN) :: nx,ny,nz
        INTEGER :: i,j,k

        DO k = 2, nz-1

                DO j = 2, ny-1

                        DO i = 2, nx-1

                               fgrid(2*i,2*j,2*k) = cgrid(i,j,k)
                               
                               fgrid(2*i-1,2*j,2*k) = 0.5d0*(cgrid(i,j,k) + cgrid(i+1,j,k))
                               fgrid(2*i,2*j-1,2*k) = 0.5d0*(cgrid(i,j,k) + cgrid(i,j+1,k))
                               fgrid(2*i,2*j,2*k-1) = 0.5d0*(cgrid(i,j,k) + cgrid(i,j,k+1))

                               fgrid(2*i-1,2*j-1,2*k) = 0.25d0*(cgrid(i,j,k) + &
                                cgrid(i+1,j,k) +  cgrid(i,j+1,k) +  cgrid(i+1,j+1,k))
                               fgrid(2*i-1,2*j,2*k-1) = 0.25d0*(cgrid(i,j,k) + &
                                cgrid(i+1,j,k) +  cgrid(i,j,k+1) +  cgrid(i+1,j,k+1))
                               fgrid(2*i,2*j-1,2*k-1) = 0.25d0*(cgrid(i,j,k) + &
                                cgrid(i,j+1,k) +  cgrid(i,j,k+1) +  cgrid(i,j+1,k+1))

                               fgrid(2*i-1,2*j-1,2*k-1) = 0.125d0*(cgrid(i,j,k) + &
                                cgrid(i+1,j,k) +  cgrid(i,j+1,k) + cgrid(i,j,k+1) + &
                                cgrid(i+1,j+1,k) + cgrid(i+1,j,k+1) + cgrid(i,j+1,k+1) &
                                + cgrid(i+1,j+1,k+1))

                        END DO

                END DO

        END DO

        END SUBROUTINE MGM_Refine

        SUBROUTINE SourceInitialize(source,nx,ny,nz)
        INTEGER, INTENT(IN) :: nx,ny,nz
        REAL*8, INTENT(INOUT) :: source(:,:,:)

        INTEGER :: i,j,k

        DO k = 1, nz

                DO j = 1, ny

                        DO i = 1, nx

                                source(i,j,k) = DEXP(-(DBLE(i - nx/2 - 0.5)/DBLE(nx/2))**2 &
                       - (DBLE(j - ny/2 - 0.5)/DBLE(ny/2))**2 - (DBLE(k - nz/2 - 0.5)/DBLE(nz/2))**2) 

!                                IF (j .EQ. 5 .AND. k .EQ. 5) PRINT*, i, source(i,j,k) 

                        END DO

                END DO 

        END DO        

        END SUBROUTINE SourceInitialize



        END PROGRAM fishie