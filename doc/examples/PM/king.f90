! 4th order Runge Kutta ODE integrator, used to integrate King Model
! distribution (Binney & Tremaine eqn.4.112)

      program main

      implicit none

      integer,parameter :: itmax = 50000000
      real*8, parameter :: PI = 4.d0*datan(1.d0)
      real*8, dimension(2) :: y0,y,bounds
      real*8 :: r0,rf,r,dr,rho,rhooldr2,rking,M0
      real*8 :: sigmasq,G,rho0,WW,Woldr2
      real*8, dimension(0:itmax) :: Mtot,psi,rhoi
      real*8, dimension(:),allocatable :: radii,speed,psiP,PHIP,potE,KE
      real*8, dimension(:,:),allocatable :: xp,vp
      real*8 :: rando(3),phi,theta,psixp,randev
      integer :: steps,i,j,np,indx,nshell
      integer,dimension(101) :: histo
      interface
         subroutine RK4(vars,del_indep,indep)
            real*8,dimension(:) :: vars
            real*8 :: indep,del_indep
         end subroutine RK4
         real*8 function rootfind(x)
            real*8, intent(in) :: x
         end function rootfind
      end interface

      common psixp,sigmasq,randev

      open(unit = 10, file = 'RK4.out')

      call random_seed()
      r0 = 0.d0
      r = r0

      dr = 1.d-6

      sigmasq = 1.d0
      G = 1.d0

      np = 2**18

      allocate (radii(np))
      allocate (speed(np))
      allocate (psiP(np))
      allocate (PHIP(np))
      allocate (potE(np))
      allocate (KE(np))
      allocate (xp(np,3))
      allocate (vp(np,3))

      !### Initial values of Psi & dPsi/dr

      y0 = (/ 3.d0*sigmasq, 0.d0 /)

      Mtot = 0.d0
      psi = 0.d0
      rhoi = 0.d0
      radii = 0.d0
      speed = 0.d0
      psiP = 0.d0
      PHIP = 0.d0
      potE = 0.d0
      KE = 0.d0
      xp = 0.d0
      vp = 0.d0

      y  = y0
      rho0 =  exp(y(1)/sigmasq)*erf(sqrt(y(1)/sigmasq)) &
         - sqrt(4.d0*y(1)/(PI*sigmasq))*(1 + 2.d0*y(1)/(3.d0*sigmasq))
      rhoi(0) = rho0
      rking = sqrt(9.d0*sigmasq/(4.d0*PI*G*rho0))
      rhooldr2 = rho0*r**2


      print *,'rho0',rho0
      print *,'rking',rking


         write(10,'(2(1x,1pe20.11E3))') r, -y(1)
         flush(10)


      i = 0
      WW = 0.d0
      Woldr2 = 0.d0
      psi(0) = y0(1)

      !### Integrate model out from center until psi -> 0

      do while ( y(1).gt. 1.d-7 )
      i = i + 1
         if (i.eq.itmax) stop
         call RK4(y,dr,r)
!         print *,r,y(1)
         write(10,'(2(1x,1pe20.11E3))') r, -y(1)
         flush(10)

      !### value of rel. potential at ith shell
         psi(i) = y(1)


      !### density in ith shell is function of rel. potential
         rho = exp(y(1)/sigmasq)*erf(sqrt(y(1)/sigmasq)) &
         - sqrt(4.d0*y(1)/(PI*sigmasq))*(1 + 2.d0*y(1)/(3.d0*sigmasq)) 
         rhoi(i) = rho


         write(19,'(3(1x,1pe20.11E3))') r/rking,rho/rho0, y(1)
         flush(19)

         !### quadrature: M_in as function of r
         Mtot(i) = Mtot(i-1) + 4.d0*PI*dr/2.d0 * (rhooldr2 + rho*r**2)

         rhooldr2 = rho*r**2
      enddo

      nshell = i-1

      rf = r
      print *,'r:', r/rking, r
      print *,'M', Mtot(nshell)
      !### total Mass of cluster
      M0 = Mtot(nshell)

      !### normalize M_in to 1, Mtot is now CDF of particle radii 
      Mtot = Mtot / M0



      do i=1, np  !###  assign particle positions and velocities
      !###  3 random deviates for 3D spatial position
        call random_number(rando)
        j = 0
        do while (rando(1).gt.Mtot(j))
          j = j+1
        enddo

      !### lin interpolate radial position of rand. dev.
      !     and value of rel. potential at that radius
        radii(i) = dble(j)*dr+dr*(rando(1)-Mtot(j))/(Mtot(j+1)-Mtot(j))
        psixp = psi(j) + (radii(i)-dr*dble(j)) * (psi(j+1)-psi(j))/dr
        psiP(i) = psixp

!        print *, dr*dble(j-1),radii(i),dr*dble(j)




      !### sphere point picking
        theta = acos(2.d0*rando(2)-1.d0)
        phi = 2.d0*PI*rando(3)

!        write(37,'(1x,1i8,3(1x,1pe20.11E3))') i, xp(i,:)

        xp(i,:) = (/ radii(i)*sin(theta)*cos(phi), &
                     radii(i)*sin(theta)*sin(phi), &
                     radii(i)*cos(theta) /)

        write(56,'(3(1x,1pe20.11E3))') xp(i,:)
        flush(56)

      !### 3 random deviates for velocity components
        call random_number(rando)



        randev = rando(1)


        bounds = (/ 0.d0, sqrt(2.d0*psixp) /)



      !### use psixp to define CDF of speed at radial position
      !    rootfind randev and CDF to find speed which that randev
      !    corresponds to
        call bisection( bounds,speed(i),rootfind )


      !### sphere point picking
        theta = acos(2.d0*rando(2)-1.d0)
        phi = 2.d0*PI*rando(3)

        vp(i,:) = (/speed(i)*sin(theta)*cos(phi), &
                    speed(i)*sin(theta)*sin(phi), &
                    speed(i)*cos(theta)/)

!        write(77,'(1x,1pe20.11E3)') speed(i)

        write(57,'(3(1x,1pe20.11E3))') vp(i,:)
        flush(57)
      enddo !###  end particle loop



      PHIP = -G*M0/rf-psiP

      potE = PHIP*M0/np
      
      print *,'PE:',0.5d0*sum(potE)

      KE = 0.5d0*speed*speed*M0/np

      print *,'KE:',sum(KE)


!      print *,maxval(speed)

!      dr = maxval(speed)/100.d0
!      histo = 0

!      do i=1,np
!         do j=1, 100
!            if( speed(i) .lt. dr*dble(j) ) exit      
!         enddo
!         histo(j) = histo(j) + 1
!      enddo

!      do i=1, 101
!         write(81,'(1x,1i4,1x,1i8)') i,histo(i)
!      enddo

      stop
      end program 

!###  SUBROUTINE RK4: performs one step of Runge Kutte integration of
!     array y(:) w.r.t. independent variable t, requires user-defined
!     function dydt(:)=f[y(:),t]

      subroutine RK4(y,dt,t)

      implicit none

      real*8, dimension(:),intent(inout) :: y
      real*8, dimension(size(y)) :: yp,k1,k2,k3,k4
      real*8, intent(in) :: dt
      real*8, intent(inout) :: t
      interface
         function dydt(vars,indep)
         real*8 :: vars(:)
         real*8 :: indep
         real*8, dimension(size(vars)) :: dydt
         end function dydt
      end interface

      k1 = dt*dydt(y,t)
      yp = y + 0.5d0*k1

      k2 = dt*dydt(yp,t+0.5d0*dt)
      yp = y + 0.5d0*k2

      k3 = dt*dydt(yp,t+0.5d0*dt)
      yp = y + k3

      k4 = dt*dydt(yp,t+dt)

      t = t+dt

      y = y + (1.d0/6.d0)*(k1 + 2.d0*k2 + 2.d0*k3 + k4)

      return
      end subroutine RK4

      subroutine bisection(bound,root,func)

      implicit none

      real*8, parameter :: eps = 1.d-5
      real*8, intent(inout),dimension(2) :: bound
      real*8, intent(out) :: root
      interface
         real*8 function func(x)
            real*8, intent(in) :: x
         end function func
      end interface
      real*8, dimension(2) :: sgns = (/ 1.d0,1.d0 /)
      real*8, dimension(2) :: eval
      real*8 :: testpt,yroot
      integer :: i
      real*8 :: psi, sigmasq,randev

      common psi,sigmasq,randev


      eval = (/ func(bound(1)) , func(bound(2)) /)

      sgns = sign(sgns,eval)

      yroot = 1.d10
      i = 0

      do while (abs(yroot) .ge. eps)
         testpt = 0.5d0*(bound(1)+bound(2))
         yroot = func(testpt)

         if (sign(sgns(1),yroot) .eq. sgns(1)) then
            bound(1) = testpt
         else
            bound(2) = testpt
         endif
         i = i + 1

         if (i.eq.100000) then
           print *,'error'
           print *,bound(1),bound(2)
           print *,testpt,yroot
           stop

         endif
      enddo      


      root = testpt
      return
      end subroutine bisection

      function dydt(y,r)
      implicit none

      real*8, parameter :: PI = 4.d0*datan(1.d0)

      real*8, dimension(:) :: y
      real*8, dimension( size(y) ) :: dydt
      real*8 :: r
      real*8 :: psixp,sigmasq,randev,G,rho1

      common psixp,sigmasq,randev

      G = 1.d0
      rho1 = 1.d0


      if (r.eq.0.d0) then
        dydt(1) = 0.d0
      else
        dydt(1) = y(2)/r**2
      endif

      dydt(2) = -4.d0*PI*G*rho1*r**2*(exp(y(1)/sigmasq)*erf(sqrt(y(1)/sigmasq)) &
      - sqrt(4.d0*y(1)/(PI*sigmasq) )*(1 + 2.d0*y(1)/(3.d0*sigmasq) ) )


      end function dydt

      real*8 function rootfind(x)
      implicit none
      real*8, parameter :: PI = 4.d0*datan(1.d0)

      real*8, intent(in) :: x
      real*8 :: psixp, sigmasq,randev
      real*8 :: C

      common psixp,sigmasq,randev

!      randev = 0.d0

      C = ((4.d0*PI)/ (2.d0*PI*sigmasq)**1.5d0 ) * 1.d0 / &
      ( exp(psixp/sigmasq)*erf(sqrt(psixp/sigmasq)) - &
      sqrt(4.d0*psixp/(PI*sigmasq))*(1.d0+2.d0*psixp/(3.d0*sigmasq)))

      rootfind = -randev + (C/6.d0)* & 
      (-2.d0*x**3 + 3.d0*sigmasq*exp(psixp/sigmasq)* &
      ( -2.d0*x*exp(-x**2/(2.d0*sigmasq)) + &
      sqrt(2.d0*PI*sigmasq)*erf(x/sqrt(2.d0*sigmasq ))))


      end function rootfind
